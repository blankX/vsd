# vsd

A Very Simple Dictionary using urbandict because I can't find a dictionary that you can use in a terminal.

## Setup
+ Install the requirement
    + `pip3 install -r requirement.txt`
+ Clone this repo (`git clone https://gitlab.com/blankX/vsd.git`)
+ cd into the repo's directory (`cd vsd`)

## Start
+ `python3 vsd.py`
+ `python3 vsd.py Hello`
